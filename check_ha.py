__author__ = 'ufik'
import csv
import urllib
import sys
import textwrap
from optparse import OptionParser



OK = 0
WARNING = 1
CRITICAL = 2


parser = OptionParser()
parser.add_option('-u', '--url', dest='url', help="-u ipaddrHaproxy")
parser.add_option('-r', '--uri', dest='uri', help="-r /stats - if default, or your uri to stats")
parser.add_option('-n', '--name',dest='name', help='name or 0,1,2,3, after launch -t show - you will see numbers of fronts and backs or their names')
parser.add_option('-o', '--opt', dest='opt', help="-o svname,bin,bout")
parser.add_option('-t', '--thelp', dest='thelp', help="more described help you get -u ipaddrHaproxy -r /statsuri -t show ")
parser.add_option('-d', '--deep',dest='dhelp', help="extended discription in name of values, key:  -d show")
parser.add_option('-w', '--warning',dest='warning', help="warning value: -w 150,100,...")
parser.add_option('-c', '--critical',dest='critical', help="critical value: -c 150,100,...")
parser.add_option('-p', '--alarm',dest='alarm', help="alarm on: -p 1 alarm of: -p 0")
(options, args) = parser.parse_args()

if not getattr(options, 'url'):
        print 'CRITICAL - %s not specified' % options.url
        raise SystemExit, CRITICAL
        sys.exit(CRITICAL)



url = 'http://' + options.url + options.uri + ";csv"
webpage = urllib.urlopen(url)
datareader = csv.reader(webpage)

if options.opt == None:
    print 'No options'
else:
    opt = options.name + ',' + options.opt
    optionsList = opt.split(",")
    if options.alarm == '1':
        noalarm = 0
        optW = options.warning
        optC = options.critical
        listWarning = optW.split(',')
        listCritical = optC.split(',')
    else:
        noalarm = 1

def parserCSV(csvdata):
    data = []
    listDict = []
    for row in csvdata:
        if row[0] == '# pxname':
            row[0] = 'pxname'
            data.append(row)
        else:
            data.append(row)
    rr = range(1,data.__len__())
    for k in rr:
        listDict.append(dict(zip(data[0], data[k])))
    return listDict

def tune(parsedData):
    rr2 = range(0,parsedData.__len__())
    j=0
    for i in rr2:
        print str(j) + ' : ' + str(parsedData[i]['pxname'] + '  ' + parsedData[i]['svname'])
        j=j+1

out = parserCSV(datareader)

deepHelp = {'pxname':'proxy name',
'svname':'service name',
'qcur':'current queued requests',
'qmax':'max queued requests',
'scur':'current sessions',
'smax':'max sessions',
'slim':'sessions limit',
'stot':'total sessions',
'bin':'bytes in',
'bout':'bytes out',
'dreq':'denied requests',
'dresp':'denied responses',
'ereq':'request errors',
'econ':'connection errors',
'eresp':'response errors',
'wretr':'retries (warning)',
'wredis':'redispatches (warning)',
'status':'status',
'weight':'server weight ',
'act':'server is active',
'bck':'server is backup',
'chkfail':'number of failed checks',
'chkdown':'number of UP->DOWN transitions',
'lastchg':'last status change (in seconds)',
'downtime':'total downtime (in seconds)',
'qlimit':'queue limit',
'pid':'process id',
'iid':'unique proxy id',
'sid':'service id',
'throttle':'warm up status',
'lbtot':'total number of times a server was selected',
'tracked':'id of proxy/server ',
'rate':'number of sessions per second over last elapsed second',
'rate_lim':'limit on new sessions per second',
'rate_max':'max number of new sessions per second',
'check_status':'status of last health check',
'check_code':'layer5-7 code, if available',
'check_duration':'time in ms took to finish last health check',
'hrsp_1xx':'http responses with 1xx code',
'hrsp_2xx':'http responses with 2xx code',
'hrsp_3xx':'http responses with 3xx code',
'hrsp_4xx':'http responses with 4xx code',
'hrsp_5xx':'http responses with 5xx code',
'hrsp_other':'http responses with other codes (protocol error)',
'hanafail':'failed health checks details',
'req_rate':'HTTP requests per second over last elapsed second',
'req_rate_max':'max number of HTTP requests per second observed',
'req_tot':'total number of HTTP requests received',
'cli_abrt':'number of data transfers aborted by the client',
'srv_abrt':'number of data transfers aborted by the server',
'comp_in':'number of HTTP response bytes fed to the compressor',
'comp_out':'number of HTTP response bytes emitted by the compressor',
'comp_byp':'number of bytes that bypassed the HTTP compressor',
'comp_rsp':'number of HTTP responses that were compressed'}


def help(qwe):
    tune(qwe)
    print 'If you have url to check stat like: "http://ipaddress/stats;csv"'
    print textwrap.dedent('''\
    You have to write ./check_ha.py -u ipaddress -r /stats -n name or number -o options
    format -n number of backaend or name FRONTEND or BACKEND like bk_nginx) -o  "option from stats"
    example by number of server: check_ha.py -u ipaddress -r /statsuri -n 1 -o svname,bin,bout
    example2 by name Front or Backend: check_ha.py -u ipaddress -r /statsuri -n ft_nginx -o svname,bin,bout
  CSV format
  0. pxname: proxy name
  1. svname: service name (FRONTEND for frontend, BACKEND for backend, any name for server)
  2. qcur: current queued requests
  3. qmax: max queued requests
  4. scur: current sessions
  5. smax: max sessions
  6. slim: sessions limit
  7. stot: total sessions
  8. bin: bytes in
  9. bout: bytes out
 10. dreq: denied requests
 11. dresp: denied responses
 12. ereq: request errors
 13. econ: connection errors
 14. eresp: response errors (among which srv_abrt)
 15. wretr: retries (warning)
 16. wredis: redispatches (warning)
 17. status: status (UP/DOWN/NOLB/MAINT/MAINT(via)...)
 18. weight: server weight (server), total weight (backend)
 19. act: server is active (server), number of active servers (backend)
 20. bck: server is backup (server), number of backup servers (backend)
 21. chkfail: number of failed checks
 22. chkdown: number of UP->DOWN transitions
 23. lastchg: last status change (in seconds)
 24. downtime: total downtime (in seconds)
 25. qlimit: queue limit
 26. pid: process id (0 for first instance, 1 for second, ...)
 27. iid: unique proxy id
 28. sid: service id (unique inside a proxy)
 29. throttle: warm up status
 30. lbtot: total number of times a server was selected
 31. tracked: id of proxy/server if tracking is enabled
 32. type (0=frontend, 1=backend, 2=server, 3=socket)
 33. rate: number of sessions per second over last elapsed second
 34. rate_lim: limit on new sessions per second
 35. rate_max: max number of new sessions per second
 36. check_status: status of last health check, one of:
        UNK     -> unknown
        INI     -> initializing
        SOCKERR -> socket error
        L4OK    -> check passed on layer 4, no upper layers testing enabled
        L4TMOUT -> layer 1-4 timeout
        L4CON   -> layer 1-4 connection problem, for example
                   "Connection refused" (tcp rst) or "No route to host" (icmp)
        L6OK    -> check passed on layer 6
        L6TOUT  -> layer 6 (SSL) timeout
        L6RSP   -> layer 6 invalid response - protocol error
        L7OK    -> check passed on layer 7
        L7OKC   -> check conditionally passed on layer 7, for example 404 with
                   disable-on-404
        L7TOUT  -> layer 7 (HTTP/SMTP) timeout
        L7RSP   -> layer 7 invalid response - protocol error
        L7STS   -> layer 7 response error, for example HTTP 5xx
 37. check_code: layer5-7 code, if available
 38. check_duration: time in ms took to finish last health check
 39. hrsp_1xx: http responses with 1xx code
 40. hrsp_2xx: http responses with 2xx code
 41. hrsp_3xx: http responses with 3xx code
 42. hrsp_4xx: http responses with 4xx code
 43. hrsp_5xx: http responses with 5xx code
 44. hrsp_other: http responses with other codes (protocol error)
 45. hanafail: failed health checks details
 46. req_rate: HTTP requests per second over last elapsed second
 47. req_rate_max: max number of HTTP requests per second observed
 48. req_tot: total number of HTTP requests received
 49. cli_abrt: number of data transfers aborted by the client
 50. srv_abrt: number of data transfers aborted by the server (inc. in eresp)
 51. comp_in: number of HTTP response bytes fed to the compressor
 52. comp_out: number of HTTP response bytes emitted by the compressor
 53. comp_byp: number of bytes that bypassed the HTTP compressor (CPU/BW limit)
 54. comp_rsp: number of HTTP responses that were compressed''')

def perfdata():
    st = ""
    st2 = ""
    rr3=range(1,len(optionsList))
    if options.dhelp == 'show':
        if noalarm == 0:
            for m in rr3:
                st = st + deepHelp[optionsList[m]] + '=' + out[int(optionsList[0])][optionsList[m]] + ';' + listWarning[m-1] + ';' + listCritical[m - 1] + '; '
                st2 = st2 + deepHelp[optionsList[m]] + '=' + out[int(optionsList[0])][optionsList[m]] + ' '
        else:
            for m in rr3:
                st2 = st2 + deepHelp[optionsList[m]] + '=' + out[int(optionsList[0])][optionsList[m]] + ' '
            st = st2
    else:
        if noalarm == 0:
            for m in rr3:
                st = st + optionsList[m] + '=' + out[int(optionsList[0])][optionsList[m]] + ';' + listWarning[m-1] + ';' + listCritical[m-1] + '; '
                st2 = st2 + optionsList[m] + '=' + out[int(optionsList[0])][optionsList[m]] + ' '
        else:
            for m in rr3:
                st2 = st2 + optionsList[m] + '=' + out[int(optionsList[0])][optionsList[m]] + ' '
            st = st2
    return st, st2

def checkStatus():
    rr3 = range(1, len(optionsList))
    for m in rr3:
        if int(out[int(optionsList[0])][optionsList[m]]) > int(listCritical[m - 1]):
            nagiosCodeReturn = CRITICAL
            status = optionsList[m] + ' CRITICAL'
            break
        elif int(out[int(optionsList[0])][optionsList[m]]) > int(listWarning[m - 1]):
            nagiosCodeReturn = WARNING
            status = optionsList[m] + ' WARNING'
            break
        else:
            if out[int(optionsList[0])]['status'] == ('OPEN'):
                status = 'OK'
                nagiosCodeReturn = OK
            elif out[int(optionsList[0])]['status'] == ('UP'):
                status = 'OK'
                nagiosCodeReturn = OK
            else:
                status = 'CRITICAL'
                nagiosCodeReturn = CRITICAL
    return status, nagiosCodeReturn






if options.thelp == 'show':
    help(out)
else:
    if str.isdigit(optionsList[0]):
        (perfData1, perfData2) = perfdata()
        if noalarm == 0:
            (status, nagiosCodeReturn) = checkStatus()
            print out[int(optionsList[0])]['pxname'] + ' ' + status + ' ' + perfData2 + '|' + perfData1
        else:
            print out[int(optionsList[0])]['pxname'] + ' ' + perfData2 + '|' + perfData1
            nagiosCodeReturn = OK
    else:
        rr3 = range(0, out.__len__())
        for a in rr3:
            if out[a]['pxname'] == optionsList[0] and out[a]['svname'] == 'FRONTEND':
                optionsList[0] = a
                break
            elif out[a]['pxname'] == optionsList[0] and out[a]['svname'] == 'BACKEND':
                optionsList[0] = a
                break
            else:
                 continue
        (perfData1, perfData2) = perfdata()
        if noalarm == 0:
            (status, nagiosCodeReturn) = checkStatus()
            print out[int(optionsList[0])]['pxname'] + ' ' + status + ' ' + perfData2 + '|' + perfData1
        else:
            print out[int(optionsList[0])]['pxname'] + ' ' + perfData2 + '|' + perfData1
            nagiosCodeReturn = OK


sys.exit(nagiosCodeReturn)








